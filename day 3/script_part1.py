print("day 3 advent of code")

file = open('input_day3.txt', 'r')

#création schéma
schema = []
for line in file:
	partie = []
	for element in line:
		partie.append(element)
	partie.pop(len(partie)-1)
	schema.append(partie)

file.close()

#tout ce qui est point ou un chiffre
#get les symbole
#remplacer les chiffre get
somme = 0
for i in range(len(schema)):
	chiffre = []
	for y in range(len(schema[i])):
		if(not(schema[i][y].isdigit()) and schema[i][y] != "."):
			print(schema[i][y])
			#cas générale
			if(i > 0 and schema[i-1][y].isdigit()):
				print(schema[i-1][y])
				valeur = []
				valeur.append(schema[i-1][y])
				schema[i-1][y] = "."
				deplacementDroite = y+1
				deplacementGauche = y-1

				while deplacementDroite < len(schema[i-1]) and schema[i-1][deplacementDroite].isdigit():
					valeur.append(schema[i-1][deplacementDroite])
					schema[i-1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i-1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i-1][deplacementGauche])
					schema[i-1][deplacementGauche] = "."
					valeur = temp + valeur
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))

			if(i < len(schema)-1 and schema[i+1][y].isdigit()):
				print(schema[i+1][y])
				valeur = []
				print(schema[i+1])
				valeur.append(schema[i+1][y])
				schema[i+1][y] = "."
				print(schema[i+1])
				deplacementDroite = y+1
				deplacementGauche = y-1

				while deplacementDroite < len(schema[i+1]) and schema[i+1][deplacementDroite].isdigit():
					valeur.append(schema[i+1][deplacementDroite])
					schema[i+1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i+1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i+1][deplacementGauche])
					valeur = temp + valeur
					schema[i+1][deplacementGauche] = "."
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))
			
			if(y > 0 and schema[i][y-1].isdigit()):
				print(schema[i][y-1])
				valeur = []
				valeur.append(schema[i][y-1])
				schema[i][y-1] = "."
				deplacementDroite = y-1 +1
				deplacementGauche = y-1 -1

				while deplacementDroite < len(schema[i]) and schema[i][deplacementDroite].isdigit():
					valeur.append(schema[i][deplacementDroite])
					schema[i][deplacementDroite] = "."
					deplacementDroite = deplacementDroite+1

				while deplacementGauche >= 0 and schema[i][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i][deplacementGauche])
					valeur = temp + valeur
					schema[i][deplacementGauche] = "."
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))
			
			if(y < len(schema[i])-1 and schema[i][y+1].isdigit()):
				print(schema[i][y+1])
				valeur = []
				valeur.append(schema[i][y+1])
				schema[i][y+1] = "."
				deplacementDroite = y+1 +1
				deplacementGauche = y+1 -1

				while deplacementDroite < len(schema[i]) and schema[i][deplacementDroite].isdigit():
					valeur.append(schema[i][deplacementDroite])
					schema[i][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i][deplacementGauche])
					valeur = temp + valeur
					schema[i][deplacementGauche] = "."
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))
			
			if(i > 0 and y > 0 and schema[i-1][y-1].isdigit()):
				print(schema[i-1][y-1])
				valeur = []
				valeur.append(schema[i-1][y-1])
				schema[i-1][y-1] = "."
				deplacementDroite = y-1 +1
				deplacementGauche = y-1 -1

				while deplacementDroite < len(schema[i-1]) and schema[i-1][deplacementDroite].isdigit():
					valeur.append(schema[i-1][deplacementDroite])
					schema[i-1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i-1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i-1][deplacementGauche])
					valeur = temp + valeur
					schema[i-1][deplacementGauche] = "."
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))

			
			if(i < len(schema)-1 and y > 0 and schema[i+1][y-1].isdigit()):
				print(schema[i+1][y-1])
				print("debug")
				valeur = []
				valeur.append(schema[i+1][y-1])
				deplacementDroite = y-1 +1
				deplacementGauche = y-1 -1
				schema[i+1][y-1] = "."

				while deplacementDroite < len(schema[i+1]) and schema[i+1][deplacementDroite].isdigit():
					valeur.append(schema[i+1][deplacementDroite])
					schema[i+1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i+1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i+1][deplacementGauche])
					valeur = temp + valeur
					schema[i+1][deplacementGauche] = "."
					deplacementGauche = deplacementGauche - 1

				print(valeur)
				somme = somme + int("".join(valeur))

			if(i < len(schema)-1 and y < len(schema[i]) and schema[i+1][y+1].isdigit()):
				print(schema[i+1][y+1])
				valeur = []
				valeur.append(schema[i+1][y+1])
				schema[i+1][y+1] = "."
				deplacementDroite = y+1 +1
				deplacementGauche = y+1 -1

				while deplacementDroite < len(schema[i+1]) and schema[i+1][deplacementDroite].isdigit():
					valeur.append(schema[i+1][deplacementDroite])
					schema[i+1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i+1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i+1][deplacementGauche])
					valeur = temp + valeur
					schema[i+1][deplacementGauche] = "."
					deplacementGauche = deplacementGauche -1 

				print(valeur)
				somme = somme + int("".join(valeur))
			
			if( i> 0 and y < len(schema[i]) -1 and schema[i-1][y+1].isdigit()):
				print(schema[i-1][y+1])
				valeur = []
				valeur.append(schema[i-1][y+1])
				schema[i-1][y+1] = "."
				deplacementDroite = y+1 +1
				deplacementGauche = y+1 -1

				while deplacementDroite < len(schema[i-1]) and schema[i-1][deplacementDroite].isdigit():
					valeur.append(schema[i-1][deplacementDroite])
					schema[i-1][deplacementDroite] = "."
					deplacementDroite = deplacementDroite + 1

				while deplacementGauche >= 0 and schema[i-1][deplacementGauche].isdigit():
					temp = []
					temp.append(schema[i-1][deplacementGauche])
					valeur = temp + valeur
					schema[i-1][deplacementGauche] = "."
					deplacementGauche = deplacementGauche -1

				print(valeur)
				somme = somme + int("".join(valeur))

print(schema)
print(somme)