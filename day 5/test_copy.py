import copy

class SetGraine: 
	start = 0
	end = 0
	decalage = 0
	resetVar = False
	isDirty = False

	def reset(self):
		self.resetVar = True

	def isReset(self):
		return self.resetVar

	def isDirty(self):
		return self.isDirty

	def impression(self):
		print("start: " + str(self.start))
		print("end: " + str(self.end))
		print("decalage: " + str(self.decalage))


test = SetGraine()
test.start = 8

bidule = SetGraine()
bidule.start = test.start

test.start = -1

test.impression()
bidule.impression()