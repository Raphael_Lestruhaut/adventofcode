print("day 1 advent of code")

class Map:
	sourceStart = 0
	sourceEnd = 0
	decalage = 0

	def impression(self):
		print("sourceStart: " + str(self.sourceStart))
		print("sourceEnd: " + str(self.sourceEnd))
		print("decalage: " + str(self.decalage))
		print("----------------------------------------------------")

class SetGraine: 
	start = 0
	end = 0
	decalage = 0
	resetVar = False
	isDirty = False

	def reset(self):
		self.resetVar = True

	def isReset(self):
		return self.resetVar

	def isDirty(self):
		return self.isDirty

	def impression(self):
		print("start: " + str(self.start))
		print("end: " + str(self.end))
		print("decalage: " + str(self.decalage))

#tableau graine
tableauGrainePasInt = []
tableauGraine = []
tableTempStart = []
tableTempEnd = []
tableauRange = []


#map
seedToSoil = []
soilToFertilizer = []
fertilizerToWater = []
waterToLight = []
lightToTemperature = []
temperatureToHumidity = []
humidityToLocation = []


#boolean
inputSeedToSoil = False
inputSoilToFertilizer = False
inputFertilizerToWater = False
inputWaterToLight = False
inputLightToTemperature = False
inputTemperatureToHumidity = False
inputHumidityToLocation = False

file = open('input_day5.txt', 'r')
somme = 0

for line in file:
	coupe = line.split(":")

	if(coupe[0] == 'seeds'):
		tableauGrainePasInt = coupe[1].split()

		for element in tableauGrainePasInt:
			tableauGraine.append(int(element))

		for i in range(len(tableauGraine)):
			if(i % 2 == 0):
				tableTempStart.append(tableauGraine[i])
		for i in range(len(tableauGraine)):
			if(i % 2 != 0):
				tableTempEnd.append(tableauGraine[i])
		for i in range(len(tableTempStart)):
			graineRange = SetGraine()
			graineRange.start = tableTempStart[i]
			graineRange.end = tableTempStart[i] + tableTempEnd[i]-1
			graineRange.decalage = 0
			tableauRange.append(graineRange)

	if(coupe[0] == 'humidity-to-location map' or inputHumidityToLocation):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = True

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			humidityToLocation.append(mapData)


	if(coupe[0] == 'temperature-to-humidity map' or inputTemperatureToHumidity):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = True
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			temperatureToHumidity.append(mapData)


	if(coupe[0] == 'light-to-temperature map' or inputLightToTemperature):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = True
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			lightToTemperature.append(mapData)

	if(coupe[0] == 'water-to-light map' or inputWaterToLight):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = True
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			waterToLight.append(mapData)

	if(coupe[0] == 'fertilizer-to-water map' or inputFertilizerToWater):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = True
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			fertilizerToWater.append(mapData)

	if(coupe[0] == 'soil-to-fertilizer map' or inputSoilToFertilizer):
		inputSeedToSoil = False
		inputSoilToFertilizer = True
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			soilToFertilizer.append(mapData)

	if(coupe[0] == 'seed-to-soil map' or inputSeedToSoil):
		inputSeedToSoil = True
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.sourceStart = int(setData[1])
			mapData.sourceEnd = int(int(setData[1]) + int(setData[2])-1)
			mapData.decalage = int(int(setData[0]) - int(setData[1]))
			seedToSoil.append(mapData)

file.close()
#les machins sont récupéré
#maintenant les trier

seedToSoil = sorted(seedToSoil, key=lambda element:element.sourceStart)
soilToFertilizer = sorted(soilToFertilizer, key=lambda element:element.sourceStart)
fertilizerToWater = sorted(fertilizerToWater, key=lambda element:element.sourceStart)
waterToLight = sorted(waterToLight, key=lambda element:element.sourceStart)
lightToTemperature = sorted(lightToTemperature, key=lambda element:element.sourceStart)
temperatureToHumidity = sorted(temperatureToHumidity, key=lambda element:element.sourceStart)
humidityToLocation = sorted(humidityToLocation, key=lambda element:element.sourceStart)

rangeLocation = []
for seedRange in tableauRange:
	
	#PARTIE SEED TO SOIL
	soil = []
	for i in range(len(seedToSoil)):
		if(seedRange.start < seedToSoil[i].sourceStart and not(seedRange.isReset())):
			valeur = SetGraine()
			valeur.start = seedRange.start
			valeur.end = seedToSoil[i].sourceStart-1
			decalage = seedRange.decalage
			soil.append(valeur)
			seedRange.start = seedToSoil[i].sourceStart

		if(seedRange.start >= seedToSoil[i].sourceStart and seedRange.start <= seedToSoil[i].sourceEnd):
			valeur = SetGraine()
			valeur.start = seedRange.start
			if(seedRange.end <= seedToSoil[i].sourceEnd):
				valeur.end = seedRange.end
			elif(seedRange.end > seedToSoil[i].sourceEnd):
				valeur.end = seedToSoil[i].sourceEnd
			valeur.decalage = seedRange.decalage + seedToSoil[i].decalage
			soil.append(valeur)
			seedRange.reset()

		if(seedRange.start > seedToSoil[i].sourceEnd):
			if(i == len(seedToSoil)-1):
				valeur = SetGraine()
				valeur.start = seedRange.start
				valeur.end = seedRange.end
				valeur.decalage = seedRange.decalage
				soil.append(valeur)
	
	for element in soil:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE PARTIE SOIL TO FERTILIZER
	fertilizer = []
	for element in soil:
		for i in range(len(soilToFertilizer)):
			if(element.start < soilToFertilizer[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = soilToFertilizer[i].sourceStart-1
				decalage = element.decalage
				fertilizer.append(valeur)
				element.start = soilToFertilizer[i].sourceStart

			if(element.start >= soilToFertilizer[i].sourceStart and element.start <= soilToFertilizer[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= soilToFertilizer[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > soilToFertilizer[i].sourceEnd):
					valeur.end = soilToFertilizer[i].sourceEnd
				valeur.decalage = element.decalage + soilToFertilizer[i].decalage
				fertilizer.append(valeur)
				element.reset()

			if(element.start > soilToFertilizer[i].sourceEnd):
				if(i == len(soilToFertilizer)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					fertilizer.append(valeur)

	for element in fertilizer:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE FERTILIZER TO WATER
	water = []
	for element in fertilizer:
		for i in range(len(fertilizerToWater)):
			if(element.start < fertilizerToWater[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = fertilizerToWater[i].sourceStart-1
				decalage = element.decalage
				water.append(valeur)
				element.start = fertilizerToWater[i].sourceStart

			if(element.start >= fertilizerToWater[i].sourceStart and element.start <= fertilizerToWater[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= fertilizerToWater[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > fertilizerToWater[i].sourceEnd):
					valeur.end = fertilizerToWater[i].sourceEnd
				valeur.decalage = element.decalage + fertilizerToWater[i].decalage
				water.append(valeur)
				element.reset()

			if(element.start > fertilizerToWater[i].sourceEnd):
				if(i == len(fertilizerToWater)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					water.append(valeur)
				
	for element in water:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE WATER TO LIGHT
	light = []
	for element in water:
		for i in range(len(waterToLight)):
			if(element.start < waterToLight[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = waterToLight[i].sourceStart-1
				decalage = element.decalage
				light.append(valeur)
				element.start = waterToLight[i].sourceStart

			if(element.start >= waterToLight[i].sourceStart and element.start <= waterToLight[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= waterToLight[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > waterToLight[i].sourceEnd):
					valeur.end = waterToLight[i].sourceEnd
				valeur.decalage = element.decalage + waterToLight[i].decalage
				light.append(valeur)
				element.reset()

			if(element.start > waterToLight[i].sourceEnd):
				if(i == len(waterToLight)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					light.append(valeur)

	for element in light:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE LIGHT TO TEMPERATURE
	temperature = []
	for element in light:
		for i in range(len(lightToTemperature)):
			if(element.start < lightToTemperature[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = lightToTemperature[i].sourceStart-1
				decalage = element.decalage
				temperature.append(valeur)
				element.start = lightToTemperature[i].sourceStart
			
			if(element.start >= lightToTemperature[i].sourceStart and element.start <= lightToTemperature[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= lightToTemperature[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > lightToTemperature[i].sourceEnd):
					valeur.end = lightToTemperature[i].sourceEnd
					element.start = lightToTemperature[i].sourceEnd + 1
				valeur.decalage = element.decalage + lightToTemperature[i].decalage
				temperature.append(valeur)
				element.reset()

			if(element.start > lightToTemperature[i].sourceEnd):
				if(i == len(lightToTemperature)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					temperature.append(valeur)

	for element in temperature:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE TEMPERATURE TO HUMIDITY
	humidity = []
	for element in temperature:
		for i in range(len(temperatureToHumidity)):
			if(element.start < temperatureToHumidity[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = temperatureToHumidity[i].sourceStart-1
				decalage = element.decalage
				humidity.append(valeur)
				element.start = temperatureToHumidity[i].sourceStart
			
			if(element.start >= temperatureToHumidity[i].sourceStart and element.start <= temperatureToHumidity[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= temperatureToHumidity[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > temperatureToHumidity[i].sourceEnd):
					valeur.end = temperatureToHumidity[i].sourceEnd
					element.start = temperatureToHumidity[i].sourceEnd + 1
				valeur.decalage = element.decalage + temperatureToHumidity[i].decalage
				humidity.append(valeur)
				element.reset()

			if(element.start > temperatureToHumidity[i].sourceEnd):
				if(i == len(temperatureToHumidity)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					humidity.append(valeur)

	for element in humidity:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	#PARTIE HUMIDITY TO LOCATION
	location = []
	for i in range(len(humidity)):
		element = humidity[i]
		for i in range(len(humidityToLocation)):

			if(element.start < humidityToLocation[i].sourceStart and not(element.isReset())):
				valeur = SetGraine()
				valeur.start = element.start
				valeur.end = humidityToLocation[i].sourceStart-1
				decalage = element.decalage
				location.append(valeur)
				element.start = humidityToLocation[i].sourceStart
			
			if(element.start >= humidityToLocation[i].sourceStart and element.start <= humidityToLocation[i].sourceEnd):
				valeur = SetGraine()
				valeur.start = element.start
				if(element.end <= humidityToLocation[i].sourceEnd):
					valeur.end = element.end
				elif(element.end > humidityToLocation[i].sourceEnd):
					valeur.end = humidityToLocation[i].sourceEnd
					element.start = humidityToLocation[i].sourceEnd + 1
				valeur.decalage = element.decalage + humidityToLocation[i].decalage
				location.append(valeur)
				element.reset()

			if(element.start > humidityToLocation[i].sourceEnd):
				if(i == len(humidityToLocation)-1):
					valeur = SetGraine()
					valeur.start = element.start
					valeur.end = element.end
					valeur.decalage = element.decalage
					location.append(valeur)

	for element in location:
		element.start = element.start + element.decalage
		element.end = element.end + element.decalage
		element.decalage = 0
		element.resetVar = False

	for element in location:
		rangeLocation.append(element)
rangeLocation = sorted(rangeLocation, key=lambda element:element.start)


print(rangeLocation[0].start)