print("day 1 advent of code")

class Map:
	destinationRangeStart = 0
	sourceRangeStart  = 0
	longeur = 0

	def impression(self):
		print("destinationRangeStart: " + str(self.destinationRangeStart))
		print("sourceRangeStart: " + str(self.sourceRangeStart))
		print("longeur: " + str(self.longeur))
		print("----------------------------------------------------")

class SeedTransport:
	soil = 0
	fertilizer = 0
	water = 0
	light = 0
	temperature = 0
	humidity = 0
	location = 0

#tableau graine
tableauGrainePasInt = []
tableauGraine = []

#map
seedToSoil = []
soilToFertilizer = []
fertilizerToWater = []
waterToLight = []
lightToTemperature = []
temperatureToHumidity = []
humidityToLocation = []


#boolean
inputSeedToSoil = False
inputSoilToFertilizer = False
inputFertilizerToWater = False
inputWaterToLight = False
inputLightToTemperature = False
inputTemperatureToHumidity = False
inputHumidityToLocation = False

file = open('input_debug.txt', 'r')
somme = 0

for line in file:
	coupe = line.split(":")

	if(coupe[0] == 'seeds'):
		tableauGrainePasInt = coupe[1].split()

		for element in tableauGrainePasInt:
			tableauGraine.append(int(element))

	if(coupe[0] == 'humidity-to-location map' or inputHumidityToLocation):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = True

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			humidityToLocation.append(mapData)


	if(coupe[0] == 'temperature-to-humidity map' or inputTemperatureToHumidity):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = True
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			temperatureToHumidity.append(mapData)


	if(coupe[0] == 'light-to-temperature map' or inputLightToTemperature):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = True
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			lightToTemperature.append(mapData)

	if(coupe[0] == 'water-to-light map' or inputWaterToLight):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = True
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			waterToLight.append(mapData)

	if(coupe[0] == 'fertilizer-to-water map' or inputFertilizerToWater):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = True
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			fertilizerToWater.append(mapData)

	if(coupe[0] == 'soil-to-fertilizer map' or inputSoilToFertilizer):
		inputSeedToSoil = False
		inputSoilToFertilizer = True
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			soilToFertilizer.append(mapData)

	if(coupe[0] == 'seed-to-soil map' or inputSeedToSoil):
		inputSeedToSoil = True
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			seedToSoil.append(mapData)

listLocation = []
for seed in tableauGraine:
	seedClasse = SeedTransport()

	for soil in seedToSoil:
		if(soil.sourceRangeStart <= seed and seed <= soil.sourceRangeStart + soil.longeur and seedClasse.soil == 0):
			if(soil.sourceRangeStart == soil.destinationRangeStart):
				seedClasse.soil = seed
			else:
				seedClasse.soil = seed + soil.destinationRangeStart - soil.sourceRangeStart
	if(seedClasse.soil == 0):
		seedClasse.soil = seed
	print("soil: " + str(seedClasse.soil))

	for fertilizer in soilToFertilizer:
		if(seedClasse.soil <= fertilizer.sourceRangeStart + fertilizer.longeur and seedClasse.soil >= fertilizer.sourceRangeStart and seedClasse.fertilizer == 0):
			if(fertilizer.sourceRangeStart == fertilizer.destinationRangeStart):
				seedClasse.fertilizer = seedClasse.soil
			else:
				seedClasse.fertilizer = seedClasse.soil + fertilizer.destinationRangeStart - fertilizer.sourceRangeStart
	if(seedClasse.fertilizer == 0):
		seedClasse.fertilizer = seedClasse.soil

	print("fertilizer: " + str(seedClasse.fertilizer))

	for water in fertilizerToWater:
		if(seedClasse.fertilizer <= water.sourceRangeStart + water.longeur and seedClasse.fertilizer >= water.sourceRangeStart and seedClasse.water == 0):
			if(water.sourceRangeStart == water.destinationRangeStart):
				seedClasse.water = seedClasse.fertilizer
			else:
				seedClasse.water = seedClasse.fertilizer + water.destinationRangeStart - water.sourceRangeStart
	if(seedClasse.water == 0):
		seedClasse.water = seedClasse.fertilizer
	print("water: " + str(seedClasse.water))

	for light in waterToLight:
		if(seedClasse.water <= light.sourceRangeStart + light.longeur and seedClasse.water >= light.sourceRangeStart and seedClasse.light == 0):
			seedClasse.light = seedClasse.water + light.destinationRangeStart - light.sourceRangeStart
	if(seedClasse.light == 0):
		seedClasse.light = seedClasse.water
	print("light: " + str(seedClasse.light))

	for temperature in lightToTemperature:
		if(seedClasse.light <= temperature.sourceRangeStart + temperature.longeur and seedClasse.light >= temperature.sourceRangeStart and seedClasse.temperature == 0):
			seedClasse.temperature = seedClasse.light + temperature.destinationRangeStart - temperature.sourceRangeStart
	if(seedClasse.temperature == 0):
		seedClasse.temperature = seedClasse.light
	print("temperature: " + str(seedClasse.temperature))
	
	for humidity in temperatureToHumidity:
		if(seedClasse.temperature <= humidity.sourceRangeStart + humidity.longeur and seedClasse.temperature >= humidity.sourceRangeStart and seedClasse.humidity == 0):
			seedClasse.humidity = seedClasse.temperature + humidity.destinationRangeStart - humidity.sourceRangeStart
	if(seedClasse.humidity == 0):
		seedClasse.humidity = seedClasse.temperature
	print("humidity: " + str(seedClasse.humidity))

	for location in humidityToLocation:
		if(seedClasse.humidity <= location.sourceRangeStart + location.longeur and seedClasse.humidity >= location.sourceRangeStart and seedClasse.location == 0):
			seedClasse.location = seedClasse.humidity + location.destinationRangeStart - location.sourceRangeStart
	if(seedClasse.location == 0):
		seedClasse.location = seedClasse.humidity
	print("location: " + str(seedClasse.location))
	
	listLocation.append(seedClasse.location)

file.close()
print(listLocation)
print(min(listLocation))