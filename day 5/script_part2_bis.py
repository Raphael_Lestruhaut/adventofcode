print("day 1 advent of code")

class Map:
	destinationRangeStart = 0
	sourceRangeStart  = 0
	longeur = 0

	def impression(self):
		print("destinationRangeStart: " + str(self.destinationRangeStart))
		print("sourceRangeStart: " + str(self.sourceRangeStart))
		print("longeur: " + str(self.longeur))
		print("----------------------------------------------------")

class SeedTransport:
	soil = 0
	fertilizer = 0
	water = 0
	light = 0
	temperature = 0
	humidity = 0
	location = 0

class RangeSeed:
	start = 0
	end = 0

	def impression(self):
		print("start: " + str(self.start))
		print("end: " + str(self.end))
		print("---------------------------------------------")


class InputReverse:
	start = 0
	end = 0

	def impression(self):
		print("start: " + str(self.start))
		print("end: " + str(self.end))

class MapGlobal:
	listMap = []

#tableau graine
tableauGrainePasInt = []
tableauGraine = []
tableauRange = []

tableTempStart = []
tableTempEnd = []

#map
seedToSoil = []
soilToFertilizer = []
fertilizerToWater = []
waterToLight = []
lightToTemperature = []
temperatureToHumidity = []
humidityToLocation = []


#boolean
inputSeedToSoil = False
inputSoilToFertilizer = False
inputFertilizerToWater = False
inputWaterToLight = False
inputLightToTemperature = False
inputTemperatureToHumidity = False
inputHumidityToLocation = False

file = open('input_debug.txt', 'r')
somme = 0

for line in file:
	coupe = line.split(":")

	if(coupe[0] == 'seeds'):
		tableauGrainePasInt = coupe[1].split()

		for element in tableauGrainePasInt:
			tableauGraine.append(int(element))

		for i in range(len(tableauGraine)):
			if(i % 2 == 0):
				tableTempStart.append(tableauGraine[i])
		for i in range(len(tableauGraine)):
			if(i % 2 != 0):
				tableTempEnd.append(tableauGraine[i])
		for i in range(len(tableTempStart)):
			graineRange = RangeSeed()
			graineRange.start = tableTempStart[i]
			graineRange.end = tableTempEnd[i]
			tableauRange.append(graineRange)

	if(coupe[0] == 'humidity-to-location map' or inputHumidityToLocation):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = True

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			humidityToLocation.append(mapData)


	if(coupe[0] == 'temperature-to-humidity map' or inputTemperatureToHumidity):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = True
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			temperatureToHumidity.append(mapData)


	if(coupe[0] == 'light-to-temperature map' or inputLightToTemperature):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = True
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False
		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			lightToTemperature.append(mapData)

	if(coupe[0] == 'water-to-light map' or inputWaterToLight):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = True
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			waterToLight.append(mapData)

	if(coupe[0] == 'fertilizer-to-water map' or inputFertilizerToWater):
		inputSeedToSoil = False
		inputSoilToFertilizer = False
		inputFertilizerToWater = True
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			fertilizerToWater.append(mapData)

	if(coupe[0] == 'soil-to-fertilizer map' or inputSoilToFertilizer):
		inputSeedToSoil = False
		inputSoilToFertilizer = True
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			soilToFertilizer.append(mapData)

	if(coupe[0] == 'seed-to-soil map' or inputSeedToSoil):
		inputSeedToSoil = True
		inputSoilToFertilizer = False
		inputFertilizerToWater = False
		inputWaterToLight = False
		inputLightToTemperature = False
		inputTemperatureToHumidity = False
		inputHumidityToLocation = False

		setData = coupe[0].split()
		if(len(setData) == 3):
			mapData = Map()
			mapData.destinationRangeStart = int(setData[0])
			mapData.sourceRangeStart = int(setData[1])
			mapData.longeur = int(setData[2])
			seedToSoil.append(mapData)

listLocation = []

entreeLocation = InputReverse()
for element in humidityToLocation:
	pluspetit = -1
	if(pluspetit == -1):
		entreeLocation.start = element.sourceRangeStart
		entreeLocation.end = element.sourceRangeStart + element.longeur
	if(pluspetit > element.destinationRangeStart):
		entreeLocation.start = element.sourceRangeStart
		entreeLocation.end = element.sourceRangeStart + element.longeur

entreeTemperature = InputReverse()
for element in temperatureToHumidity:
	if(element.destinationRangeStart <= entreeLocation.start and element.destinationRangeStart + element.longeur >= entreeLocation.end):
		entreeTemperature.start = element.sourceRangeStart
		entreeTemperature.end = element.sourceRangeStart + element.longeur
	elif(element.destinationRangeStart >= entreeLocation.start and element.destinationRangeStart + element.longeur >= entreeLocation.end):
		entreeTemperature.start = element.sourceRangeStart
		entreeTemperature.end = entreeLocation.end
	elif(element.destinationRangeStart <= entreeLocation.start and element.destinationRangeStart + element.longeur <= entreeLocation.end and element.destinationRangeStart + element.longeur >= entreeLocation.start):
		entreeTemperature.start = entreeLocation.start
		entreeTemperature.end = element.sourceRangeStart + element.longeur
	else:
		entreeTemperature = entreeLocation

entreLight = InputReverse()
for element in lightToTemperature:
	print("destinationRangeStart:" + str(element.destinationRangeStart))
	print("start:" + str(entreeTemperature.start))
	print("destinationRangeStart + longeur: " + str(element.destinationRangeStart + element.longeur))
	print("end:" + str(entreeTemperature.end))
	if(element.destinationRangeStart <= entreeTemperature.start and entreeTemperature.end <= element.destinationRangeStart + element.longeur):
		print("if")
		entreLight.start = element.sourceRangeStart
		entreLight.end = element.sourceRangeStart + element.longeur
	elif(element.destinationRangeStart >= entreeTemperature.start and element.destinationRangeStart + element.longeur >= entreeTemperature.end):
		print("elif")
		entreLight.start = element.sourceRangeStart
		entreLight.end = entreeTemperature.end
	elif(element.destinationRangeStart <= entreeLocation.start and element.destinationRangeStart + element.longeur <= entreeTemperature.end and element.destinationRangeStart + element.longeur >= entreeTemperature.end):
		print("elif 2")
		entreLight.start = entreeTemperature.start
		entreLight.end = element.sourceRangeStart + element.longeur
	else:
		print("else")
		entreLight = entreeTemperature
entreLight.impression()

file.close()