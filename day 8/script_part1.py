print("day 1 advent of code")

class CheminIterable: 
	iterationBoucle = 0
	emplacementZ = []

file = open('input_day8.txt', 'r')

somme = 0
firstLine = True
chemin = ""
listNode = {}
for line in file:

	if(firstLine):
		chemin = line
		firstLine = False

	else:
		coupure = line.split("=")
		
		if(len(coupure) == 2):
			start = coupure[0].strip()
			print(start)

			suite = coupure[1].split(",")
			tempLeft = suite[0].strip()
			tempLeft = tempLeft[1:4]
			left = tempLeft

			tempRight = suite[1].strip()
			tempRight = tempRight[0:3]
			right = tempRight

			listNode[start] = (left, right)
file.close()

compteurStep = 0
depart = listNode["LPA"]
zNotFound = True

while zNotFound:
	suite = ""
	for char in chemin:
		if(char == "L"):
			suite = depart[0]
			depart = listNode[depart[0]]
			compteurStep = compteurStep + 1
			if(suite == "ZZZ"):
				zNotFound = False
		elif(char == "R"):
			suite = depart[1]
			depart = listNode[depart[1]]
			compteurStep = compteurStep + 1
			if(suite == "ZZZ"):
				zNotFound = False

print(compteurStep)