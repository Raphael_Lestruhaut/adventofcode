print("day 1 advent of code")

class Node:
	start = ""
	left = ""
	right = ""

	def impression(self):
		print("start: " + self.start + " left: " + self.left + " right: " + self.right)
		print("-----------------------------------------------------------------------")

class CheminIterable: 
	iterationBoucle = 0
	emplacementZ = []

file = open('input_day8.txt', 'r')

somme = 0
firstLine = True
chemin = ""
listNode = []
for line in file:

	if(firstLine):
		chemin = line
		firstLine = False

	else:
		coupure = line.split("=")
		
		if(len(coupure) == 2):
			tempNode = Node()
			tempNode.start = coupure[0].strip()

			suite = coupure[1].split(",")
			tempLeft = suite[0].strip()
			tempLeft = tempLeft[1:4]
			tempNode.left = tempLeft

			tempRight = suite[1].strip()
			tempRight = tempRight[0:3]
			tempNode.right = tempRight

			listNode.append(tempNode)

file.close()

#trouver la first step
compteur = 0
depart = []
for element in listNode:
	if(element.start[2] == "A"):
		depart.append(element)

listeChemin = []
compteurStep = 0

val = depart[0]
depart = []
depart.append(val)

for element in depart:
	print("test")
	cheminBoucle = CheminIterable()
	startBoucle = element
	elementCourant = Node()
	stepBoucle = 0
	while elementCourant.start != startBoucle.start and elementCourant.right != startBoucle.right and elementCourant.left != startBoucle.left:

		if(stepBoucle == 0):
			elementCourant.start = startBoucle.start
			elementCourant.right = startBoucle.right
			elementCourant.left = startBoucle.left
		for char in chemin:
			if(char == "L"):
				suite = elementCourant.left
				suiteRecherche = True,
				rechercheCompteur = 0
				while suiteRecherche:
					if(listNode[rechercheCompteur].start == suite):
						elementCourant = listNode[rechercheCompteur]
						suiteRecherche = False
						stepBoucle = stepBoucle + 1
					rechercheCompteur = rechercheCompteur + 1
				
				if(suite[2] == "Z"):
					cheminBoucle.emplacementZ.append(stepBoucle)

			elif(char == "R"):
				suite = elementCourant.right
				suiteRecherche = True
				rechercheCompteur = 0
				while suiteRecherche:
					if(listNode[rechercheCompteur].start == suite):
						elementCourant = listNode[rechercheCompteur]
						suiteRecherche = False
						stepBoucle = stepBoucle + 1 
					rechercheCompteur = rechercheCompteur + 1
				if(suite[2] == "Z"):
					cheminBoucle.emplacementZ.append(stepBoucle)

	cheminBoucle.stepBoucle = stepBoucle
	listeChemin.append(cheminBoucle)
	print("boucle finis")

print(compteurStep)