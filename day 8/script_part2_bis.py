import math
print("day 8 advent of code")

class CheminIterable: 
	iterationBoucle = 0
	emplacementZ = []

file = open('input_day8.txt', 'r')

somme = 0
firstLine = True
chemin = ""
listNode = {}
listNodeDepart = {}
for line in file:

	if(firstLine):
		chemin = line
		firstLine = False

	else:
		coupure = line.split("=")
		
		if(len(coupure) == 2):
			start = coupure[0].strip()

			suite = coupure[1].split(",")
			tempLeft = suite[0].strip()
			tempLeft = tempLeft[1:4]
			left = tempLeft

			tempRight = suite[1].strip()
			tempRight = tempRight[0:3]
			right = tempRight
			listNode[start] = (left, right)
			if(start[2] == "A"):
				listNodeDepart[start] = (left, right)

file.close()

tableauBoucle = []

for element in listNodeDepart:
	nonBoucle = True
	depart = listNode[element]
	compteurStep = 0
	
	while nonBoucle:
		suite = ""
		for char in chemin:
			if(char == "L"):
				suite = depart[0]
				depart = listNode[depart[0]]
				if(suite[2] == "Z"):
					compteurStep = compteurStep + 1
					nonBoucle = False
					tableauBoucle.append(compteurStep)
				elif(nonBoucle): 
					compteurStep = compteurStep + 1
			elif(char == "R"):
				suite = depart[1]
				depart = listNode[depart[1]]
				if(suite[2] == "Z"):
					compteurStep = compteurStep + 1
					nonBoucle = False
					tableauBoucle.append(compteurStep)
				elif(nonBoucle):
					compteurStep = compteurStep + 1

multiplication = 1
notEgaux = True

for element in tableauBoucle:
	multiplication = multiplication * element 

diviseur = []
reste = []
for element in tableauBoucle:
	resteTemp = multiplication%element
	reste.append(resteTemp)
	rond = multiplication - resteTemp
	diviseur.append(rond/element)

test = 1
for element in diviseur:
	test = test * element

print("bidule: " + str(math.lcm(tableauBoucle[0], tableauBoucle[1], tableauBoucle[2], tableauBoucle[3], tableauBoucle[4], tableauBoucle[5])))
print(reste)
print(diviseur)
print(tableauBoucle)