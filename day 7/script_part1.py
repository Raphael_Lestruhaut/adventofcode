print("day 7 advent of code")

class Hand: 
	carte = ""
	valeur = 0
	gain = 0

	#NOTE IMPORTANTE:
	#-1 pas trouvé
	#0 high card
	#1 one pair
	#2 two pair
	#3 three of a kind
	#4 full house
	#5 four of a kind
	#6 five of a kind
	pattern = -1 

file = open('input_day7.txt', 'r')

tableauHighCard = []
tableauOnePair = []
tableauTwoPair = []
tableauThreeKind = []
tableauFullHoute = []
tableauFourKind = []
tableauFiveKind = []

def toCarte(carte):
	if(carte == 'T'):
		return "10"
	elif(carte == 'J'):
		return "11"
	elif(carte == "Q"):
		return "12"
	elif(carte == "K"):
		return "13"
	elif(carte == "A"):
		return "14"
	else:
		return carte

for line in file:
	coupure = line.split()
	
	count = [0] * 15
 
	for element in coupure[0]:
		match element:
			case '1':
				count[0] = count[0] + 1
			case '2':
				count[1] = count[1] + 1
			case '3':
				count[2] = count[2] + 1
			case '4':
				count[3] = count[3] + 1
			case '5':
				count[4] = count[4] + 1
			case '6':
				count[5] = count[5] + 1
			case '7':
				count[6] = count[6] + 1
			case '8':
				count[7] = count[7] + 1
			case '9':
				count[8] = count[8] + 1
			case 'T':
				count[9] = count[9] + 1
			case 'J':
				count[10] = count[10] + 1
			case 'Q':
				count[11] = count[11] + 1
			case 'K':
				count[12] = count[12] + 1
			case 'A':
				count[13] = count[13] + 1
	
	if(count.count(5)==1):
		tempHand = Hand()
		tempHand.pattern = 6
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauFiveKind.append(tempHand)

	if(count.count(4)==1):
		tempHand = Hand()
		tempHand.pattern = 5
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauFourKind.append(tempHand)

	if(count.count(3) == 1 and count.count(2) == 1):
		tempHand = Hand()
		tempHand.pattern = 4
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauFullHoute.append(tempHand)

	if(count.count(3) == 1 and count.count(2) == 0):
		tempHand = Hand()
		tempHand.pattern = 4
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauThreeKind.append(tempHand)

	if(count.count(2) == 2):
		tempHand = Hand()
		tempHand.pattern = 4
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauTwoPair.append(tempHand)

	if(count.count(2) == 1 and count.count(3) == 0):
		tempHand = Hand()
		tempHand.pattern = 4
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauOnePair.append(tempHand)

	if(count.count(1) == 5):
		tempHand = Hand()
		tempHand.pattern = 4
		tempHand.carte = coupure[0]
		
		puissance = ""
		for carte in coupure[0]:
			puissance = puissance + str(int(toCarte(carte)) + 10)
		
		tempHand.valeur = int(puissance)
		tempHand.gain = int(coupure[1])
		tableauHighCard.append(tempHand)

file.close()


tableauHighCard = sorted(tableauHighCard, key=lambda element:element.valeur)
tableauOnePair = sorted(tableauOnePair, key=lambda element:element.valeur)
tableauTwoPair = sorted(tableauTwoPair, key=lambda element:element.valeur)
tableauThreeKind = sorted(tableauThreeKind, key=lambda element:element.valeur)
tableauFullHoute = sorted(tableauFullHoute, key=lambda element:element.valeur)
tableauFourKind = sorted(tableauFourKind, key=lambda element:element.valeur)
tableauFiveKind = sorted(tableauFiveKind, key=lambda element:element.valeur)

valeurFinal = 0
compteur = 1
for element in tableauHighCard:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1 

for element in tableauOnePair:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

for element in tableauTwoPair:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

for element in tableauThreeKind:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

for element in tableauFullHoute:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

for element in tableauFourKind:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

for element in tableauFiveKind:
	valeurFinal = valeurFinal + (element.gain * compteur)
	compteur = compteur + 1

print("valeur final: " + str(valeurFinal))
