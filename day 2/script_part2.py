print("day 2 advent of code")

class Bag:
	maxBlue = 0
	maxRouge = 0
	maxVert = 0
	numero = 0

	def setNumero(self, numero):
		self.numero = numero

	def addBlue(self, blue):
		if(blue > self.maxBlue):
			self.maxBlue = blue

	def addRouge(self, rouge):
		if(rouge > self.maxRouge):
			self.maxRouge = rouge

	def addVert(self, vert):
		if(vert > self.maxVert):
			self.maxVert = vert

	def printInformation(self):
		print("maxBlue: " + str(self.maxBlue))
		print("maxRouge: " + str(self.maxRouge))
		print("maxVerte: " + str(self.maxVert))
		print("numero: " + str(self.numero))

	def sommeCube(self):
		return self.maxBlue * self.maxRouge * self.maxVert

file = open('input_day2.txt', 'r')
somme = 0
for line in file:
	sac = Bag()
	separationBase = line.split(':')
	idGame = separationBase[0]
	sac.setNumero(idGame.split()[1]) 
	
	tirage = separationBase[1].split(';')
	for cube in tirage:
		element = cube.split(',')
		
		for valeur in element:
			valeurStrip = valeur.strip()
			partition = valeurStrip.split()
			
			if(partition[1] == "red"):
				sac.addRouge(int(partition[0]))
			elif(partition[1] == "green"):
				sac.addVert(int(partition[0]))
			elif(partition[1] == "blue"):
				sac.addBlue(int(partition[0]))

	somme = somme + sac.sommeCube()

file.close()
print(somme)