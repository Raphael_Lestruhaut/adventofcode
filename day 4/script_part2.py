print("day 4 advent of code")

class Carte:
	numeroCarte = 0
	nombreCarte = 0
	
	def impression(self):
		print("---------------------")
		print("numero carte: " + str(self.numeroCarte))
		print("nombre carte: " + str(self.nombreCarte))

file = open('input_day4.txt', 'r')

compteurCarte = 1
listeCarte = []
for line in file:
	carteTemp = Carte()
	carteTemp.numeroCarte = compteurCarte
	compteurCarte = compteurCarte + 1
	listeCarte.append(carteTemp)

file.close()

file = open('input_day4.txt', 'r')

compteurIdCarte = 0
for line in file:
	listeCarte[compteurIdCarte].nombreCarte = listeCarte[compteurIdCarte].nombreCarte + 1

	coupure = line.split(":")
	valeur = coupure[1].split("|")
	nombreGagnant = valeur[0].split()
	nombreTrouver = valeur[1].split()
	sommeVictoire = 0
	for valeur in nombreTrouver:
		for nombreWinner in nombreGagnant:
			if(valeur == nombreWinner):
				sommeVictoire = sommeVictoire + 1 

	#process
	for i in range(listeCarte[compteurIdCarte].nombreCarte):
		derive = compteurIdCarte + 1
		for i in range(sommeVictoire):
			listeCarte[derive+i].nombreCarte = listeCarte[derive+i].nombreCarte + 1

	compteurIdCarte = compteurIdCarte + 1

file.close()

sommeCarte = 0
for carte in listeCarte:
	sommeCarte = sommeCarte + carte.nombreCarte

print(sommeCarte)