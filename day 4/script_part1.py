print("day 4 advent of code")

file = open('input_day4.txt', 'r')
somme = 0
for line in file:
	sommeLine = 0
	coupure = line.split(":")
	valeur = coupure[1].split("|")

	nombreGagnant = valeur[0].split()
	nombreTrouver = valeur[1].split()
	print(nombreGagnant)
	print(nombreTrouver)
	for valeur in nombreTrouver:
		for nombreWinner in nombreGagnant:
			if(valeur == nombreWinner):
				if(sommeLine == 0):
					sommeLine = 1
				else:
					sommeLine = sommeLine * 2
	somme = somme + sommeLine

file.close()
print(somme)