print("day 6 advent of code")

file = open('input_day6.txt', 'r')
timeDistance = []
somme = 0
compteur = 0
for line in file:
	coupure = line.split(":")
	valeur = coupure[1].split()
	timeDistance.append(valeur)

tableVictoire = []
for i in range(len(timeDistance[0])):
	distance = timeDistance[1][i]
	time = timeDistance[0][i]
	compteurVictoire = 0
	for i in range(int(time)):
		if(i != 0):
			if(((int(time) - i) * i) > int(distance)):
				compteurVictoire = compteurVictoire + 1

	tableVictoire.append(compteurVictoire)

valeurFinal = 1
for element in tableVictoire:
	valeurFinal = valeurFinal * element
file.close()
print(valeurFinal)