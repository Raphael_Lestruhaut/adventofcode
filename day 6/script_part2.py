print("day 6 advent of code")

file = open('input_day6.txt', 'r')
timeDistance = []
somme = 0
compteur = 0
for line in file:
	coupure = line.split(":")
	valeur = coupure[1].split()
	timeDistance.append(valeur)

file.close()

timeRace = int("".join(timeDistance[0]))
distanceRace = int("".join(timeDistance[1]))

tableVictoire = []
compteurVictoire = 0
for i in range(timeRace):
	if(i != 0):
		if(((timeRace - i) * i) > distanceRace):
			compteurVictoire = compteurVictoire + 1

print(compteurVictoire)