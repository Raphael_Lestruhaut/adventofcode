print("day 1 advent of code")

class Valeur:
	indexFirstNumber = -1
	indexLastNumber = -1
	firstNumber = 0
	lastNumber = 0

	def newNumber(self, index, number):
		if(self.indexLastNumber == -1 and self.indexFirstNumber == -1):
			self.indexFirstNumber = index
			self.firstNumber = number
			self.indexLastNumber = index
			self.lastNumber = number
		elif(index < self.indexFirstNumber):
			self.indexFirstNumber = index
			self.firstNumber = number
		elif(index > self.indexLastNumber):
			self.indexLastNumber = index
			self.lastNumber = number

	def resultatLigne(self):
		return (self.firstNumber * 10) + self.lastNumber

	def printLine(self):
		print("firstNumber: " + str(self.firstNumber))
		print("lastNumber: " + str(self.lastNumber))
		print("indexFirstNumber: " + str(self.indexFirstNumber))
		print("indexLastNumber:" + str(self.indexLastNumber))
		print("sommeLigne:" + str(self.resultatLigne()))

def rechercheFormeLitteral(line, numberLitteral, number, sommeLigne):
	indexValue = line.find(numberLitteral)
	temp = list(line)
	for i in range(len(numberLitteral)):
		temp[line.find(numberLitteral)+i] = str(number)
	temp = "".join(temp)
	sommeLigne.newNumber(indexValue, number)

	while temp.find(numberLitteral) > -1:
		chaineTemp = temp
		indexValue = chaineTemp.find(numberLitteral)
		temp = list(temp)
		for i in range(len(numberLitteral)):
			temp[chaineTemp.find(numberLitteral)+i] = str(number)
		temp = "".join(temp)
		sommeLigne.newNumber(indexValue, number)

file = open('day1_input.txt', 'r')
somme = 0
for line in file:
	listNumber = []
	compteur = 0
	sommeLigne = Valeur()

	if(line.find('one') > -1):
		rechercheFormeLitteral(line, 'one', 1, sommeLigne)
	if(line.find('two') > -1):
		rechercheFormeLitteral(line, 'two', 2, sommeLigne)
	if(line.find('three') > -1):
		rechercheFormeLitteral(line, 'three', 3, sommeLigne)
	if(line.find('four') > -1):
		rechercheFormeLitteral(line, 'four', 4, sommeLigne)
	if(line.find('five') > -1):
		rechercheFormeLitteral(line, 'five', 5, sommeLigne)
	if(line.find('six') > -1):
		rechercheFormeLitteral(line, 'six', 6, sommeLigne)
	if(line.find('seven') > -1):
		rechercheFormeLitteral(line, 'seven', 7, sommeLigne)
	if(line.find('eight') > -1):
		rechercheFormeLitteral(line, 'eight', 8, sommeLigne)
	if(line.find('nine') > -1):
		rechercheFormeLitteral(line, 'nine', 9, sommeLigne)

	for char in line:

		if(char.isdigit()):
			sommeLigne.newNumber(compteur, int(char))
		compteur = compteur + 1
	somme = somme + int(sommeLigne.resultatLigne())

file.close()
print(somme)